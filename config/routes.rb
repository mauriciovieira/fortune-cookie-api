Rails.application.routes.draw do
  root to: 'fortunes#draw'
  get 'draw', to: 'fortunes#draw'
end
