class FortunesController < ApplicationController
  # GET /draw
  def draw
    rand = Random.rand(Fortune.all.size)
    @fortune = Fortune.find(rand)
    render json: @fortune
  end
end
