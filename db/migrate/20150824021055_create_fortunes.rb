class CreateFortunes < ActiveRecord::Migration
  def change
    create_table :fortunes do |t|
      t.string :phrase

      t.timestamps
    end
    add_index :fortunes, :phrase, unique: true
  end
end
